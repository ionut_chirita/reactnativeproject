// src/components/Product.js
import React from 'react';
import { Text, StyleSheet, View } from 'react-native';
import { Card, Button } from 'react-native-elements';
import { withNavigation } from 'react-navigation';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faMapMarkerAlt, faClock } from '@fortawesome/free-solid-svg-icons';
import { TouchableOpacity } from 'react-native';
import Moment from 'moment';

class Product extends React.Component {

    showDate(date) {
        Moment.locale('ro');
        return Moment.unix(date).format('d MMM');
    }
    render() {
        Moment.locale('ro')
        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate('Details', {
                id: this.props.product.id,
                title: this.props.product.title,
                priceCombined: this.props.product.priceCombined,
                firstImage: this.props.product.firstImage,
                location: this.props.product.location, 
                approved_at: this.props.product.approved_at,
                token: this.props.token,
              })}>
            <Card style={styles.card}>
                <Card.Image source={{ uri: this.props.product.firstImage }}></Card.Image>
                <Text style={styles.price} h4>
                    {this.props.product.priceCombined}
                </Text>
                <Text style={styles.name} h2>
                    {this.props.product.title}
                </Text>

                <Text h6 style={styles.description}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam...
                </Text>

                <View style={{ flex: 1, flexWrap: 'wrap', flexDirection: 'row', marginTop: 10, marginBottom: 10 }}>
                    <View style={styles.location}>
                        <FontAwesomeIcon style={{ color: '#c1c4cd' }} icon={faMapMarkerAlt} />
                        <Text style={{ color: '#c1c4cd', marginLeft: 3 }}>{this.props.product.location}</Text></View>
                    <View style={styles.date}>
                        <Text style={{ color: '#c1c4cd', marginRight: 10 }}>{this.showDate(this.props.product.approved_at)}</Text>
                        <FontAwesomeIcon style={{ color: '#c1c4cd' }} icon={faClock} />
                    </View>
                </View>

            </Card>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    card: {
        padding: 0,
        margin: 0,
        width: 100,

    },
    name: {
        color: '#5a647d',
        fontWeight: 'bold',
        fontSize: 15,
        marginBottom: 5,
        marginTop: 5,
    },
    price: {
        fontWeight: 'bold',
        marginBottom: 10,
        color: '#009688',
        fontSize: 15,
        paddingTop: 10,
        minWidth: 10
    },
    description: {
        fontSize: 13,
        color: '#c1c4cd'
    },
    location: {
        alignSelf: 'flex-start',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    date: {
        flex: 1,
        alignSelf: 'flex-end',
        alignItems: 'center',
        justifyContent: 'flex-end',
        flexDirection: 'row',
        flexWrap: 'wrap',
        color: '#c1c4cd'

    }
});

export default withNavigation(Product);