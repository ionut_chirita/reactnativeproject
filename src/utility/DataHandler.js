import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { withNavigation } from 'react-navigation';

class DataHandler extends React.Component {

    storeData = async (value) => {
        try {
          await AsyncStorage.setItem('@token', value)
        } catch (e) {
          // saving error
        }
      }

      getData = async () => {
        try {
          const value = await AsyncStorage.getItem('@token')
          if(value !== null) {
            // value previously stored
          }
        } catch(e) {
          // error reading value
        }
      }

}


export default withNavigation(DataHandler);
