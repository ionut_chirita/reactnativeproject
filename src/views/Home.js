// src/views/Home.js
import React from 'react';
import { View, ActivityIndicator, StyleSheet,ScrollView } from 'react-native';
import { withNavigation } from 'react-navigation';
import Product from '../components/Product';

class HomeScreen extends React.Component {

    state = {
        productsF: [],
        loading: true,
        token: '',
    }

    async componentDidMount() {


        try {
            const tokenApiCall = await fetch('https://lajumate.ro/test-web/ionut/create-token', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'public-key': 'VXGNq3GcR73gNJ2FLN2cMZwHPPujy71yrveK0scORS7KzoXWVyjXjh6vzZ1spvzg',
                },
    
            });
    
            const response = await tokenApiCall.json();
            console.log(response);
            this.setState({token: response.token});
        } catch(err) {
            console.log("Error fetching data-----------", err);
        }

        try {
            const productsApiCall = await fetch('https://lajumate.ro/test-web/ionut/search-items?loggedIn=1&version=2&source=web', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'public-key': 'VXGNq3GcR73gNJ2FLN2cMZwHPPujy71yrveK0scORS7KzoXWVyjXjh6vzZ1spvzg',
                    'token': `${this.state.token}`
                },

            });

            const response = await productsApiCall.json();
            this.setState({productsF: response.ads, loading: false});
        } catch(err) {
            console.log("Error fetching data-----------", err);
        }


    }


    render() {

        const { productsF, loading, token } = this.state;

        if(!loading) {
            return (
                <ScrollView
                style={{
                    flexGrow: 0,
                    width: "100%",
                    height: "100%",
                }}>
                {
                    productsF.map((product, index) => {
                    return(
                        <View style={styles.row} key={index}>
                            <View style={styles.col}>
                            <Product product={product} token={token}/>
                            </View>
                        </View>
                    )
                    })
                }
                </ScrollView>
            );
            } else {
                return <ActivityIndicator />
            }



    }
}

const styles = StyleSheet.create({
  row: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'center',
  },
  col: {
      flex: 1,
  },
});

export default withNavigation(HomeScreen);
