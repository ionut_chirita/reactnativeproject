// src/views/Details.js
import React from 'react';
import { View, Text, ActivityIndicator, ScrollView, StyleSheet } from 'react-native';
import Product from '../components/Product';
import { Card } from 'react-native-elements';

class DetailsScreen extends React.Component {

    state = {
        productF: {},
        loading: true
    }

    async componentDidMount() {

        try {

            const productApiCall = await fetch(`https://lajumate.ro/test-web/ionut/item-details/${this.props.navigation.getParam('id')}?loggedIn=1&version=2&source=web`, {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'public-key': 'VXGNq3GcR73gNJ2FLN2cMZwHPPujy71yrveK0scORS7KzoXWVyjXjh6vzZ1spvzg',
                    'token': `${this.props.navigation.getParam('token')}`
                },

            });

            const response = await productApiCall.json();
            //console.log(response.ad);
            this.setState({productF: response.ad, loading: false});
        } catch(err) {
            console.log("Error fetching data-----------", err);
        }
    }

    render() {
        
        const { productF, loading } = this.state;
        const { navigation } = this.props;
        const product =   {
            title: navigation.getParam('title'),
            priceCombined: navigation.getParam('price'),
            firstImage: navigation.getParam('firstImage'),
            location: navigation.getParam('location'),
            approved_at: navigation.getParam('approved_at'),
        };


          if(!loading) {
            return (
                <ScrollView
                style={{
                    flexGrow: 0,
                    width: "100%",
                    height: "100%",
                }}>
                    <View>
                        <View >
                            <Product product={product}/>
                        </View>
                        <Card>
                        <Text style={styles.description} h2>
                            Descriere
                        </Text>
                        <Text h6>

                            {productF.description}
                            
                        </Text>
                        </Card>
                    </View>
                </ScrollView>
            );
            } else {
                return <ActivityIndicator />
            }
    }
}

const styles = StyleSheet.create({

    description: {
        fontSize: 13,
        color: '#c1c4cd'
    },
});

export default DetailsScreen;
